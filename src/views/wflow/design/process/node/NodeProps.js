//审批节点属性
export const APPROVAL_NODE_PROPS = {

}

//办理节点属性
export const TASK_NODE_PROPS = {

}

//分支节点属性
export const BRANCH_NODE_PROPS = {
  type: 'SINGLE', //分支类型，SINGLE 排他条件，MORE 满足的条件均执行，ALL 并行执行所有的

}

//抄送节点属性
export const CC_NODE_PROPS = {
  type: ''
}

export default {
  APPROVAL_NODE_PROPS, TASK_NODE_PROPS, BRANCH_NODE_PROPS,
  CC_NODE_PROPS
}


